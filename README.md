EPS8266 DHT11 InfluxDB Collector
-------------
Read temperature and humidity with an ESP8266 and DHT11 and push the data to an InfluxDB server

# Requirements
You will need to install PlatformIO for your platform. This README explains the usage of the PlatformIO CLI.

You will also need a compatible board: [List of compatible boards](https://platformio.org/boards).

# Recommendations
This project was created with a NodeMCU 1.0 compatible board. I would recommend you to use a similar board.

# Installation
1. Connect your ESP8266 via USB
2. Make sure all necessary drivers are installed for your board
   - I am using a NodeMCU 1.0 compatible board for this
3. Initialize the PlatformIO project with your board
   - You may need a different name for your board. Just look yours up with `$ pio boards <name>`
   - If you want to use this project in CLion or any other IDE you can do that by adding the option `--ide`
   - See `$ pio init --help` for more information.
   - Example for CLI only and a NodeMCU 1.0 compatbile: `$ pio init -b nodemcuv2`
4. Edit the configuration in `src/Settings.ino` to your liking
5. Now you can build and upload the binary to your board: `$ pio run -t upload`
   - Dependencies will be automatically downloaded
## Quickstart
Run the following commands in shell to upload the program to a NodeMCU 1.0 compatible board:
```shell script
pio init -b nodemcuv2
pio run -t upload
```

# License
This project is licensed under the terms of the GNU General Public License v3.0. See [LICENSE](LICENSE)

The code in `lib/ESP8266_Inflxudb_Scrumplex` is based on original code from [Tobias Schürg](https://github.com/tobiasschuerg),
which is licensed under the terms of the MIT License.

See [ESP8266_Influx_DB](https://github.com/tobiasschuerg/ESP8266_Influx_DB) Repository on GitHub.
