/*
 *   ESP8266 DHT11 InfluxDB Collector
 *   Copyright (C) 2020 Sefa Eyeoglu <contact@scrumplex.net (https://scrumplex.net)
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>

#include <Adafruit_Sensor.h>

#include <InfluxDb.h>
#include "DHT.h"

#include "Settings.ino"


char counter = 0;

Influxdb influx(INFLUXDB_HOST, INFLUXDB_PORT);
DHT dht(DHTPIN, DHTTYPE);

void setup() {
    Serial.begin(9600);
    Serial.println();
    Serial.print("connecting to ");
    Serial.println(WIFI_SSID);
    WiFi.mode(WIFI_STA);
    WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
        Serial.print(".");
    }
    Serial.println("");
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());

    influx.setDb(INFLUXDB_DATABASE);

    dht.begin();
}

void loop() {
    delay(2000);
    Serial.println(F("Pulling DHT sensor data..."));

    float h = dht.readHumidity();
    float t = dht.readTemperature();
    if (isnan(h) || isnan(t)) {
        Serial.println(F("Failed to read from DHT sensor!"));
        return;
    }

    Serial.print(F("Humidity: "));
    Serial.print(h);
    Serial.print(F("%  Temperature: "));
    Serial.print(t);
    Serial.println(F("°C"));


    if (counter >= 5) {
        Serial.println(F("Inserting into database..."));
        boolean success = influx.write();
        if (success) {
            counter = 0;
            Serial.println(F("Successfully inserted data sets into database"));
        }
    }


    InfluxData humidity(INFLUXDB_MEASUREMENT_HUMIDITY_NAME);
    humidity.addTag("room", INFLUXDB_ROOM_TAG);
    humidity.addValue("value", h);
    influx.prepare(humidity);

    InfluxData temp(INFLUXDB_MEASUREMENT_TEMPERATURE_NAME);
    temp.addTag("room", INFLUXDB_ROOM_TAG);
    temp.addValue("value", t);
    influx.prepare(temp);

    counter++;
}
